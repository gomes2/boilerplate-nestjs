import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { SequelizeModule } from '@nestjs/sequelize';
import config from './config';

import { UsersController } from './modules/users';

@Module({
  imports: [
    SequelizeModule.forRoot({
      dialect: 'mysql',
      host: config.mysql.host,
      port: config.mysql.port,
      username: config.mysql.username,
      password: config.mysql.password,
      database: config.mysql.database,
      models: [`${__dirname}./../**/**.entity{.ts,.js}`]
    }),
    ConfigModule.forRoot()
  ],
  controllers: [UsersController],
  providers: [] // Put services here
})
export class AppModule {}
