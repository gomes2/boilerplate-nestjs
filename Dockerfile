FROM node:12

# Create Directory for the Container
WORKDIR /usr/src/app

RUN echo "America/Sao_Paulo" > /etc/timezone
RUN dpkg-reconfigure -f noninteractive tzdata

# Install app dependencies
COPY package*.json ./

RUN npm install

EXPOSE 3012

CMD ["npm", "run", "start:dev"]